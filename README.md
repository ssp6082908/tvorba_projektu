# Úvodní nadpis tohoto dokumentu první úrovně

## Nadpis druhé úrovně

**tučně** *italic*

Použite příkaz `import numpy as np` 

```python

def funkce(a, b):
	result = a + b
	return result
	#TODO
```

Barva #F3FFE3

Odkaz najdete zde [GitLab](https://www.gitlab.com/).

List:

* první položka
* druhá položka
* třetí položka

1. První
1. Druhá 
1. Třetí
	1.třetí zase
		* položka
			- položka

- [x] Hotovo
- [ ] Splň

@marded


